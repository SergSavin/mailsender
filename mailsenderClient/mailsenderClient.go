//GO implementation of MailSenderAPI
//API
/*
Auth is authentication key betweemn client and server

To register new mail account for sending emails:
/Register
POST FORM
{
	Auth          	string
	Server 			string
	Port   			int
	Login      		string
	Password   		string
	Proto 			string 	"IMAP"|"SMTP"
}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}

/DeleteAcc
POST FORM
{
	Auth          	string
	Login      		string
	Password      	string
}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}


/SendMail
POST FORM
{
	Auth               string
	From               string   //display email
	To                 []string //mail recepients
	DefaultMailAccount string   //prefered email to send the mail
	UseOthersAccounts  bool     //if false - the email will be send onle from DefaultMailAccount (doesnt matter if it failed). If true - other accounts will be used if sending from the DefaultMailAccount fail
	Subject            string   //subject of mail
	Body               string   //mail body
}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}

/GetStat
POST FORM
{
	Auth               string
	Login 				string     	//account for stat
	From               string   	//date in YYYY-MM-DD
	To                 string 		//date in YYYY-MM-DD
}

Result 200 JSON
[]TMailStat struct {
	Date  string
	To    string
	Count int
	Error string
}


/GetRegisteredAccounts
POST FORM
{
	Auth               string
}
Result 200 JSON
{
	[]TMailAccount struct {
		Server   string
		Port     int
		Login    string
		Proto    string //IMAP || SMTP
	},
	{
		Error string
	}
}
*/
package mailsenderClient

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	MailSenderServer   = "http://0.0.0.0:7788"
	MailServerPassword = "TheVeryInterestingPassword"
)

type TMail struct {
	From               string   //display email
	To                 []string //mail recepients
	DefaultMailAccount string   //prefered email to send the mail
	UseOthersAccounts  bool     //if false - the email will be send onle from DefaultMailAccount (doesnt matter if it failed). If true - other accounts will be used if sending from the DefaultMailAccount fail
	Subject            string   //subject of mail
	Body               string   //mail body
}
type TMailStat struct {
	Date  string
	To    string
	Count int
	Error string
}

type TMailAccount struct {
	Server   string
	Port     int
	Login    string
	Password string
	Proto    string //IMAP || SMTP
	Error    string
}

func SendMail(m TMail) string {
	postform := prepareMail(m)
	return send2Server(postform)
}
func Register(a TMailAccount) error {
	if !(a.Proto == "SMTP" || a.Proto == "IMAP") {
		return errors.New("Proto should be one of IMAP||SMTP!")
	}
	urlV := url.Values{
		"Auth":     {MailServerPassword},
		"Server":   {a.Server},
		"Port":     {strconv.Itoa(a.Port)},
		"Login":    {a.Login},
		"Password": {a.Password},
		"Proto":    {a.Proto},
	}
	resp, err := httpClient.PostForm(MailSenderServer+"/Register", urlV)
	if err != nil {
		return err
	}
	lBodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var e TError
	err = json.Unmarshal(lBodyData, &e)
	if err != nil {
		return err
	}
	if e.Error != "" {
		return errors.New(e.Error)
	}
	return nil
}
func GetStat(m TMailAccount, from, to time.Time) TMailStat {
	var r TMailStat
	urlV := url.Values{
		"Auth":  {MailServerPassword},
		"Login": {m.Login},
		"From":  {from.Format("2006-01-02")},
		"To":    {to.Format("2006-01-02")},
	}
	resp, err := httpClient.PostForm(MailSenderServer+"/GetStat", urlV)
	if err != nil {
		r.Error = err.Error()
		return r
	}
	lBodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		r.Error = err.Error()
		return r
	}
	err = json.Unmarshal(lBodyData, &r)
	if err != nil {
		r.Error = err.Error()
		return r
	}
	return r
}
func GetRegisteredAccounts() (TMailAccount, error) {
	var r TMailAccount
	urlV := url.Values{
		"Auth": {MailServerPassword},
	}
	resp, err := httpClient.PostForm(MailSenderServer+"/GetRegisteredAccounts", urlV)
	if err != nil {
		return r, err
	}
	lBodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return r, err
	}
	err = json.Unmarshal(lBodyData, &r)
	if err != nil {
		return r, err
	}
	return r, nil
}
func DeleteAcc(a TMailAccount) error {
	urlV := url.Values{
		"Auth":     {MailServerPassword},
		"Login":    {a.Login},
		"Password": {a.Password},
	}
	resp, err := httpClient.PostForm(MailSenderServer+"/DeleteAcc", urlV)
	if err != nil {
		return err
	}
	lBodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var e TError
	err = json.Unmarshal(lBodyData, &e)
	if err != nil {
		return err
	}
	if e.Error != "" {
		return errors.New(e.Error)
	}
	return nil
}

///EOF PUBLIC DECLARATIONS
var httpClient http.Client

func prepareMail(m TMail) map[string][]string {
	res := make(map[string][]string)
	res = url.Values{
		"Auth":               {MailServerPassword},
		"From":               {m.From},
		"To":                 {strings.Join(m.To, ",")},
		"DefaultMailAccount": {m.DefaultMailAccount},
		"UseOthersAccounts":  {strconv.FormatBool(m.UseOthersAccounts)},
		"Subject":            {m.Subject},
		"Body":               {m.Body},
	}
	return res
}
func send2Server(postform map[string][]string) string {
	var res string

	resp, err := httpClient.PostForm(MailSenderServer+"/SendMail", url.Values(postform))
	if err != nil {
		res = err.Error()
		return JSONMarshal(res)
	}
	lBodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		res = err.Error()
		return JSONMarshal(res)
	}
	return string(lBodyData)
}
func JSONMarshal(s string) string {
	var e TError
	r, _ := json.Marshal(e)
	return string(r)
}

type TError struct {
	Error string
}
