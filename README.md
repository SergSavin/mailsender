# mailsender

REPO for mailsender project
# API for simple sending mails from any service 

Auth is authentication key between client and server
All bad requests as well as response with errors are with statuscode 400

To register new mail account for sending emails:
# /Register
POST FORM
{
	Auth          	string
	Server 			string
	Port   			int
	Login      		string
	Password   		string
	Proto 			string 	"IMAP"|"SMTP"
}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}

# /SendMail
POST FORM
{
	Auth               string
	From               string   //display email
	To                 []string //mail recepients
	DefaultMailAccount string   //prefered email to send the mail
	UseOthersAccounts  bool     //if false - the email will be send onle from DefaultMailAccount (doesnt matter if it failed). If true - other accounts will be used if sending from the DefaultMailAccount fail
	Subject            string   //subject of mail
	Body               string   //mail body
}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}

# /GetStat
POST FORM
{
	Auth               string
	From               string   	//date in YYYY-MM-DD
	To                 string 		//date in YYYY-MM-DD
}
Result 200 JSON
[]TMailStat struct {
	Date  string
	To    string
	Count int
	Error string
}


# /GetRegisteredAccounts
POST FORM
{
	Auth               string
}
Result 200 JSON
{
	[]TMailAccount struct {
		Server   string
		Port     int
		Login    string
		Proto    string //IMAP || SMTP
	},
	{
		Error string
	}
}


// All data is cached in the memory struct *TDataStorage thats flushing data to disk (*.csv) every minute

// FOR PRODUCTION (MySQL or Redis storage) THE ONLY THING TO DO IS TO REWRITE
// func (a *TDataStorage) loadStat() ([]TMailStat, error) {
// func (a *TDataStorage) loadAcc() ([]TMailAccount, error) {
// func (a *TDataStorage) Store() {

