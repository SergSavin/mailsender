//GO implementation of MailSenderAPI
//API
/*
Auth is authentication key betweemn client and server
All bad requests as well as response with errors are with statuscode 400

To register new mail account for sending emails:
/Register
POST FORM
{
	Auth          	string
	Server 			string
	Port   			int
	Login      		string
	Password   		string
	Proto 			string 	"SMTP" | "IMAP" STILL IN TEST

}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}

/SendMail
POST FORM
{
	Auth               string
	From               string   //display email
	To                 []string //mail recepients
	DefaultMailAccount string   //prefered email to send the mail
	UseOthersAccounts  bool     //if false - the email will be send onle from DefaultMailAccount (doesnt matter if it failed). If true - other accounts will be used if sending from the DefaultMailAccount fail
	Subject            string   //subject of mail
	Body               string   //mail body
}
Result 200 JSON {"Error":"EMPTY OR ERROR DESCRIPTION"}

/GetStat
POST FORM
{
	Auth               string
	From               string   	//date in YYYY-MM-DD
	To                 string 		//date in YYYY-MM-DD
}
Result 200 JSON
[]TMailStat struct {
	Date  string
	To    string
	Count int
	Error string
}


/GetRegisteredAccounts
POST FORM
{
	Auth               string
}
Result 200 JSON
{
	[]TMailAccount struct {
		Server   string
		Port     int
		Login    string
		Proto    string //IMAP || SMTP
	},
	{
		Error string
	}
}
*/

// All data is cached in the memory struct *TDataStorage thats flushing data to disk (*.csv) every minute

// FOR PRODUCTION (MySQL or Redis storage) THE ONLY THING TO DO IS TO REWRITE
// func (a *TDataStorage) loadStat() ([]TMailStat, error) {
// func (a *TDataStorage) loadAcc() ([]TMailAccount, error) {
// func (a *TDataStorage) Store() {

package main

import (
	"encoding/json"
	"errors"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

const MailServerPassword = "TheVeryInterestingPassword"

var (
	datastorage *TDataStorage
	mailsender  chan TMailSender //All mails will be send one by one from channel
)

type TMail struct {
	From               string   //display email
	To                 []string //mail recepients
	DefaultMailAccount string   //prefered email to send the mail
	UseOthersAccounts  bool     //if false - the email will be send onle from DefaultMailAccount (doesnt matter if it failed). If true - other accounts will be used if sending from the DefaultMailAccount fail
	Subject            string   //subject of mail
	Body               string   //mail body
}
type TMailStat struct {
	From       string
	DatetimeGo time.Time
	To         string
	Count      int
	Error      string
}

type TMailAccount struct {
	Server   string
	Port     int
	Login    string
	Password string
	Proto    string //IMAP || SMTP
	Error    string
}
type TMailSender struct {
	acc TMailAccount
	m   TMail
}

//for easy json marshaling errors
type TError struct {
	Error string
}

func main() {

	mailsender = make(chan TMailSender, 256)

	var err error
	//set up start port of the service
	var port int
	flag.IntVar(&port, "port", 7788, "port to start http Server")
	// get cur dir
	curDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	// create log dir in the same dir
	logDir := filepath.Join(curDir, "logs")
	if err := os.MkdirAll(logDir, 0777); err != nil {
		panic(err.Error())
	}

	//LOGGER
	log.SetOutput(&lumberjack.Logger{
		Filename:   filepath.Join(logDir, "normal.log"),
		MaxSize:    10, // megabytes
		MaxBackups: 7,
		MaxAge:     7, //days
	})
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// create log for panic
	panicFileName := filepath.Join(logDir, "panic.log")
	var panicLog *os.File
	if _, err := os.Stat(panicFileName); err != nil {
		if panicLog, err = os.OpenFile(panicFileName, os.O_WRONLY|os.O_CREATE|os.O_SYNC, 0644); err != nil {
			log.Println(err.Error())
			return
		}
	} else {
		if panicLog, err = os.OpenFile(panicFileName, os.O_WRONLY|os.O_SYNC, 0644); err != nil {
			log.Println(err.Error())
			return
		}
	}
	syscall.Dup2(int(panicLog.Fd()), 1)
	syscall.Dup2(int(panicLog.Fd()), 2)

	//graceful shutdown
	gracefulStop := make(chan os.Signal, 2)
	signal.Notify(gracefulStop, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGABRT, syscall.SIGALRM)
	go func() {
		for {
			sig := <-gracefulStop
			if sig == syscall.SIGHUP {
				log.Println("got signal SIGHUP. Ignored!")
			} else {
				log.Printf("got signal: %+v. Exiting...", sig)
				os.Exit(0)
			}
		}
	}()

	datastorage, err = Create("accstorage.csv", "statstorage.csv")
	if err != nil {
		log.Fatalln("Cant load storage from disk!", err)
	}

	go MailSender()
	mux := http.NewServeMux()
	mux.HandleFunc("/Register", register)
	mux.HandleFunc("/DeleteAcc", deleteacc)
	mux.HandleFunc("/SendMail", sendmail)
	mux.HandleFunc("/GetStat", getstat)
	mux.HandleFunc("/GetRegisteredAccounts", getregisteredaccounts)
	log.Fatalln(http.ListenAndServe("127.0.0.1:"+strconv.Itoa(port), mux))
}

//func to register new mail acc. Checks the account by logging into it
func register(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err == nil && r.Form.Get("Auth") == MailServerPassword {
		var acc TMailAccount
		acc.Server = r.Form.Get("Server")
		acc.Port, err = strconv.Atoi(r.Form.Get("Port"))
		if err != nil {
			http.Error(w, "invalid data in Port", 400)
			return
		}
		acc.Login = r.Form.Get("Login")
		acc.Password = r.Form.Get("Password")
		acc.Proto = r.Form.Get("Proto")
		if err := IsValidAcc(acc); err == nil {
			err := datastorage.AddAcc(acc)
			if err == nil {
				http.Error(w, JSONMarshal("Ok!"), 200)
			} else {
				http.Error(w, JSONMarshal(err.Error()), 400)
			}
		} else {
			http.Error(w, JSONMarshal(err.Error()), 400)
		}
	} else {
		http.Error(w, JSONMarshal("unauthorized request!"), 400)
		return
	}
}
func deleteacc(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err == nil && r.Form.Get("Auth") == MailServerPassword {
		var acc TMailAccount
		acc.Login = r.Form.Get("Login")
		acc.Password = r.Form.Get("Password")
		err := datastorage.DeleteAcc(acc)
		if err != nil {
			http.Error(w, JSONMarshal(err.Error()), 400)
		} else {
			http.Error(w, JSONMarshal("OK!"), 200)
		}
	} else {
		http.Error(w, JSONMarshal("unauthorized request!"), 400)
		return
	}
}
func sendmail(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err == nil && r.Form.Get("Auth") == MailServerPassword {
		defaultMailAccount := r.Form.Get("DefaultMailAccount")
		if macc, err := datastorage.GetAcc(defaultMailAccount); err != nil {
			http.Error(w, JSONMarshal(err.Error()), 400)
		} else {
			var m TMail
			m.From = r.Form.Get("From")
			m.To = strings.Split(r.Form.Get("To"), ",")
			m.Subject = r.Form.Get("Subject")
			m.Body = r.Form.Get("Body")
			m.UseOthersAccounts, err = strconv.ParseBool(r.Form.Get("UseOthersAccounts"))
			if len(m.To) > 0 && (len(m.Subject) > 0 || len(m.Body) > 0) {
				//all info seems good. Add to queue
				var ms TMailSender
				ms.acc = macc
				ms.m = m
				mailsender <- ms
				http.Error(w, JSONMarshal("Ok!"), 200)
			}
		}
	} else {
		http.Error(w, JSONMarshal("unauthorized request!"), 400)
		return
	}
}
func getstat(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err == nil && r.Form.Get("Auth") == MailServerPassword {
		login := r.Form.Get("Login")
		datefrom := r.Form.Get("From")
		dateto := r.Form.Get("To")
		if login != "" && datefrom != "" && dateto != "" {
			stat, err := datastorage.GetAccStat(login, datefrom, dateto)
			if err != nil {
				http.Error(w, JSONMarshal(err.Error()), 400)
			} else {
				b, e := json.Marshal(stat)
				if err != nil {
					http.Error(w, JSONMarshal(e.Error()), 400)
				} else {
					http.Error(w, string(b), 200)
				}
			}
		} else {
			http.Error(w, JSONMarshal("not enough data!"), 400)
		}

	} else {
		http.Error(w, JSONMarshal("unauthorized request!"), 400)
		return
	}

}
func getregisteredaccounts(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err == nil && r.Form.Get("Auth") == MailServerPassword {
		b, e := json.Marshal(datastorage.GetALLAcc())
		if e != nil {
			http.Error(w, JSONMarshal(e.Error()), 400)
		} else {
			http.Error(w, string(b), 200)
		}
	} else {
		http.Error(w, JSONMarshal("unauthorized request!"), 400)
		return
	}
}
func IsValidAcc(m TMailAccount) error {
	if m.Login == "" || m.Password == "" || m.Port < 0 || m.Port > 9999 || m.Server == "" || (m.Proto != "SMTP") { //m.Proto != "IMAP" &&
		return errors.New("Empty data in credentials!")
	}
	//try to connect to email
	if m.Proto == "SMTP" {
		return CheckSMTPAcc(m.Server, m.Port, m.Login, m.Password)
	}
	if m.Proto == "IMAP" {
		return CheckIMAPAcc(m.Server, m.Port, m.Login, m.Password)
	}
	return nil
}
func JSONMarshal(s string) string {
	var e TError
	e.Error = s
	r, _ := json.Marshal(e)
	return string(r)
}
func MailSender() {
	for {
		newtask := <-mailsender
		if newtask.acc.Proto == "IMAP" {
			err := SendIMAPMail(newtask.acc, newtask.m)
			for j := range newtask.m.To {
				var mstat TMailStat
				mstat.DatetimeGo = time.Now()
				mstat.From = newtask.acc.Login
				mstat.To = newtask.m.To[j]
				mstat.Count = 1
				if err != nil {
					mstat.Error = err.Error()
				}
				datastorage.AddStatData(mstat)
			}
		} else if newtask.acc.Proto == "SMTP" {
			err := SendSMTPMail(newtask.acc, newtask.m)
			for j := range newtask.m.To {
				var mstat TMailStat
				mstat.DatetimeGo = time.Now()
				mstat.From = newtask.acc.Login
				mstat.To = newtask.m.To[j]
				mstat.Count = 1
				if err != nil {
					mstat.Error = err.Error()
				}
				datastorage.AddStatData(mstat)
			}
		}
	}
}
