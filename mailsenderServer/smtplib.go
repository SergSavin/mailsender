package main

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/mail"
	"net/smtp"
	"strconv"
)

func CheckSMTPAcc(server string, port int, login string, password string) error {
	// Connect to the SMTP Server

	auth := smtp.PlainAuth("", login, password, server)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         server,
	}
	conn, err := tls.Dial("tcp", server+":"+strconv.Itoa(port), tlsconfig)
	if err != nil {
		return err
	}

	c, err := smtp.NewClient(conn, server)
	if err != nil {
		return err
	}
	defer c.Quit()
	// Auth
	if err = c.Auth(auth); err != nil {
		return err
	}
	return nil
}
func SendSMTPMail(a TMailAccount, m TMail) error {
	for i := range m.To {
		err := smptpmail(a.Login, a.Password, a.Server+":"+strconv.Itoa(a.Port), m.To[i], m.Subject, m.Body)
		if err != nil {
			return err
		}
	}
	return nil
}
func smptpmail(frommail, pswd, servernamewithport string, tomail string, sbj, msg string) error {

	from := mail.Address{"", frommail}
	to := mail.Address{"", tomail}
	body := msg

	// Setup headers
	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = sbj

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	// Connect to the SMTP Server
	servername := servernamewithport

	host, _, _ := net.SplitHostPort(servername)

	auth := smtp.PlainAuth("", frommail, pswd, host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		return err
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		return err
	}

	// Auth
	if err = c.Auth(auth); err != nil {
		return err
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		return err
	}

	if err = c.Rcpt(to.Address); err != nil {
		return err
	}

	// Data
	w, err := c.Data()
	if err != nil {
		return err
	}
	//message = "truetrue"
	_, err = w.Write([]byte(message))
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}
	c.Quit()
	return nil
}
