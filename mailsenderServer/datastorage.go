package main

import (
	"encoding/csv"
	"errors"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

type TDataStorage struct {
	m           sync.Mutex
	accpath     string
	statpath    string
	AccountData []TMailAccount
	StatData    []TMailStat
}

//Create new data structure. If files exists -> load data from files otherwise create empty
func Create(accpath, statpath string) (*TDataStorage, error) {
	var d TDataStorage
	var err error
	d.accpath = accpath
	d.statpath = statpath
	if fileExists(d.accpath) {
		d.AccountData, err = d.loadAcc()
		if err != nil {
			return &d, err
		}
	} else {
		err := createFile(d.accpath)
		if err != nil {
			return &d, err
		}
	}
	if fileExists(d.statpath) {
		d.StatData, err = d.loadStat()
		if err != nil {
			return &d, err
		}
	} else {
		err := createFile(d.accpath)
		if err != nil {
			return &d, err
		}
	}
	//regularly flush data to disk and delete very old data
	go d.work()
	return &d, nil
}

//Store data to disk
//TODO IN THE PRODUCTION STORING MAY BE TO MySQL or Redis
func (a *TDataStorage) Store() {
	//store AccountsData
	a.m.Lock()
	defer a.m.Unlock()
	file, err := os.Create(a.accpath)
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for i := range a.AccountData {
		var value []string
		value = append(value, a.AccountData[i].Server, strconv.Itoa(a.AccountData[i].Port), a.AccountData[i].Login, a.AccountData[i].Password, a.AccountData[i].Proto)
		err := writer.Write(value)
		if err != nil {
			log.Println(err)
			return
		}
	}
	//store StatData
	file2, err := os.Create(a.statpath)
	if err != nil {
		log.Println(err)
		return
	}
	defer file2.Close()

	writer2 := csv.NewWriter(file2)
	defer writer2.Flush()

	for i := range a.StatData {
		var value []string
		value = append(value, a.StatData[i].From, a.StatData[i].DatetimeGo.Format("2006-01-02 03:04:05"), a.StatData[i].To, a.StatData[i].Error)
		err := writer2.Write(value)
		if err != nil {
			log.Println(err)
			return
		}
	}

}

//Get Account data by its email login
func (a *TDataStorage) GetAcc(login string) (TMailAccount, error) {
	a.m.Lock()
	defer a.m.Unlock()
	for i := range a.AccountData {
		if a.AccountData[i].Login == login {
			return a.AccountData[i], nil
		}
	}
	return TMailAccount{}, errors.New("No such account in DB!")
}

//Get ALL Accounts
func (a *TDataStorage) GetALLAcc() []TMailAccount {
	a.m.Lock()
	defer a.m.Unlock()
	var r []TMailAccount
	for i := range a.AccountData {
		r = append(r, a.AccountData[i])
	}
	return r
}

//Add new Account data
func (a *TDataStorage) AddAcc(m TMailAccount) error {
	a.m.Lock()
	defer a.m.Unlock()
	for i := range a.AccountData {
		if a.AccountData[i].Login == m.Login {
			//there is such acc in the DB
			return errors.New("There is such account in the DB! Delete old account first!")
		}
	}
	a.AccountData = append(a.AccountData, m)
	return nil
}

//Delete Account from the DB
func (a *TDataStorage) DeleteAcc(m TMailAccount) error {
	a.m.Lock()
	defer a.m.Unlock()
	for i := range a.AccountData {
		if (a.AccountData[i].Login == m.Login) && (a.AccountData[i].Password == m.Password) {
			//delete it
			a.AccountData[i] = a.AccountData[len(a.AccountData)-1]
			a.AccountData = a.AccountData[:len(a.AccountData)-1]
			return nil
		}
	}
	return errors.New("No such acc in the DB!")
}

//Get Stat data by its email login
func (a *TDataStorage) GetAccStat(login string, datefrom, dateto string) ([]TMailStat, error) {
	var r []TMailStat
	datefromGo, err := time.Parse("2006-01-02 03:04:05", datefrom)
	if err != nil {
		return r, err
	}
	datetoGo, err := time.Parse("2006-01-02 03:04:05", dateto)
	if err != nil {
		return r, err
	}
	a.m.Lock()
	defer a.m.Unlock()
	for i := range a.StatData {
		if a.StatData[i].From == login && a.StatData[i].DatetimeGo.Before(datetoGo) && datefromGo.Before(a.StatData[i].DatetimeGo) {
			r = append(r, a.StatData[i])
		}
	}
	return r, nil
}

//Add Stat Data
func (a *TDataStorage) AddStatData(m TMailStat) error {
	a.m.Lock()
	defer a.m.Unlock()
	a.StatData = append(a.StatData, m)
	return nil
}

//Regularly flush data to disk
func (a *TDataStorage) work() {
	for {
		time.Sleep(time.Minute)
		a.Store()
		a.clearOldStat()
	}
}

//delete stat that is older than 1 month to prevent disk/mem usage
func (a *TDataStorage) clearOldStat() {
	a.m.Lock()
	defer a.m.Unlock()
	for i := range a.StatData {
		if a.StatData[i].DatetimeGo.Before(time.Now().Add(-24 * 30 * time.Hour)) {
			a.StatData[i] = a.StatData[len(a.StatData)-1]
			a.StatData = a.StatData[:len(a.StatData)-1]
		}
	}
}

//TODO IN THE PRODUCTION LOADING MAY BE FROM MySQL or Redis
func (a *TDataStorage) loadAcc() ([]TMailAccount, error) {
	var m []TMailAccount
	// Open CSV file
	f, err := os.Open(a.accpath)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		log.Fatalln(err)
	}

	// Loop through lines & turn into object
	for _, line := range lines {
		p, e := strconv.Atoi(line[1])
		if e != nil {
			return m, e
		}
		data := TMailAccount{
			Server:   line[0],
			Port:     p,
			Login:    line[2],
			Password: line[3],
			Proto:    line[4],
		}
		m = append(m, data)
	}
	return m, nil
}

//TODO IN THE PRODUCTION LOADING MAY BE FROM MySQL or Redis
func (a *TDataStorage) loadStat() ([]TMailStat, error) {
	var m []TMailStat

	// Open CSV file
	f, err := os.Open(a.statpath)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		panic(err)
	}

	// Loop through lines & turn into object
	for _, line := range lines {
		datetimeGo, err := time.Parse("2006-01-02 03:04:05", line[1])
		if err != nil {
			return m, err
		}

		data := TMailStat{
			From:       line[0],
			DatetimeGo: datetimeGo,
			To:         line[2],
			Error:      line[3],
		}
		if err != nil {
			return m, err
		}
		m = append(m, data)
	}
	return m, nil
}
func fileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}
func createFile(name string) error {
	file, err := os.Create(name)
	if err != nil {
		return err
	}
	defer func() {
		file.Close()
	}()
	return nil
}
