package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/mail"
	"net/smtp"
	"strconv"
	"time"

	"github.com/emersion/go-imap/client"
)

func CheckIMAPAcc(ImapServer string, ImapPort int, login string, password string) error {
	// Connect to server
	dialer := net.Dialer{Timeout: time.Second * 10}
	c, err := client.DialWithDialerTLS(&dialer, ImapServer+":"+strconv.Itoa(ImapPort), nil)
	if err != nil {
		return err
	}
	defer c.Logout()
	// Login
	if err := c.Login(login, password); err != nil {
		return err
	}
	return nil
}
func SendIMAPMail(a TMailAccount, m TMail) error {
	for i := range m.To {
		err := imapmail(a.Login, a.Password, a.Server+":"+strconv.Itoa(a.Port), m.To[i], m.Subject, []byte(m.Body))
		if err != nil {
			return err
		}
	}
	return nil
}
func imapmail(frommail, pswd, servernamewithport, tomail, sbj string, msg []byte) error {

	from := mail.Address{frommail, frommail}
	to := mail.Address{tomail, tomail}
	body := string(msg)

	// Setup headers
	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = sbj
	headers["Content-Type"] = "text/html; charset=utf-8"
	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	// Connect to the SMTP Server
	servername := servernamewithport

	host, _, err := net.SplitHostPort(servername)
	if err != nil {
		return err
	}
	auth := smtp.PlainAuth("", frommail, pswd, host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		log.Println(err)
		return err
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		log.Println(err, host)
		return err
	}
	defer c.Quit()
	// Auth
	if err = c.Auth(auth); err != nil {
		log.Println(err)
		return err
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		log.Println(err)
		return err
	}

	if err = c.Rcpt(to.Address); err != nil {
		log.Println(err)
		return err
	}

	// Data
	w, err := c.Data()
	if err != nil {
		log.Println(err)
		return err
	}
	//message = "truetrue"
	_, err = w.Write([]byte(message))
	if err != nil {
		log.Println(err)
		return err
	}

	err = w.Close()
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
